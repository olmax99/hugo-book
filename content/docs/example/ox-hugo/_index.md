+++
title = "gohugo (ox-hugo)"
author = ["Admin User"]
draft = false
+++

A powerful static site generator written in Go. Hugo has native .org file
support. For even more control there are other options out there:

-   [https://ox-hugo.scripter.co/](https://ox-hugo.scripter.co/)
-   Go to: [the plain org-mode version]({{< ref "/content/docs/example/orgmode/_index.org" >}} "org") to view the similar page in *org-mode*.

Find more useful resources

-   [https://github.com/theNewDynamic/awesome-hugo](https://github.com/theNewDynamic/awesome-hugo)

{{< imgproc sunset Resize "300x" />}}

## Useful Commands {#useful-commands}

```nil
$ hugo mod init <path/to/project>               ;
```


## Getting Started {#getting-started}

-   Hugo modules evolve around the all defined components.
-   Component types defined in Hugo:
    1.  static
    2.  content
    3.  layouts
    4.  data
    5.  assets
    6.  i18n
    7.  archetypes

| Service | IP                          | Hostname                                       |
|---------|-----------------------------|------------------------------------------------|
| Consul  | <http://192.168.0.111:8500> | <http://consul.service.lab.consul:8500>        |
| Vault   | <http://192.168.0.111:8200> | <http://active.vault.service.lab.consul:8200>  |
| Nomad   | <http://192.168.0.111:4646> | <http://nomad-servers.service.lab.consul:4646> |
| Traefik | <http://192.168.0.111:8081> |                                                |

[{{<figure src="/ox-hugo/gopher.png" caption="data transit" width="50%">}}](images/logo.png)

{{< hint info >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

{{< hint warning >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

### New Docs {#new-docs}

-   Quickly start a new project for technical docs
-   <span class="underline">NOTE</span>: The test content itself shows all the `md` syntax required for a technical
    documentation. On top there are two general modes:
    1.  Docs
    2.  Posts
-   Even the _WARNING: Expand shortcode is deprecated. Use 'details' instead._ is
    actually part of the documentation, as it is shown how to drop warnings from
    inside a theme: `[Create warnings: {{ warnf "... <my warning>" }}]`

{{< highlight go >}}
{{< readfile file="/_vendor/github.com/alex-shpak/hugo-book/layouts/shortcodes/expand.html" markdown="true" >}}
{{< /highlight >}}

-   All other warnings are simply dead links (probably for copyright reasons)

```bash
$ hugo new site <my-docs> --format yaml
$ git init                                 ## ... setup new git repo
..
$ hugo mod init <gitlab.com/user/project>

## in config.yaml
# theme: github.com/alex-shpak/hugo-book

$ hugo mod tidy
$ hugo mod vendor

## Get all resources from ExampleSite
# cp -r ../hugo-book-original/exampleSite/* .

$ hugo server -D
```


### Page Bundles {#page-bundles}

-   _Page Bundles_ are a form of creating components of content units
-   Each _page bundle_ contains an `_index.md`


### Hugo Modules {#hugo-modules}

-

```bash
$ hugo mod get -u ./...        ; update recursivly
$ hugo mod graph
```


## Understand Themes {#understand-themes}

-   From Scratch: [https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/](https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/)
-   We will use the example of [theNewDynamic/gohugo-theme-ananke](https://github.com/theNewDynamic/gohugo-theme-ananke) theme.
-   The theme will map the base directory structure from the project base
    directory into the `_vendor/github.com/.../<theme>` directory.


### Concepts {#concepts}

-   <span class="underline">scss</span>: Simply an introduction of variables into legacy css. Slightly different
    syntax.


#### 1. Partials {#1-dot-partials}

-   Partials are small, context-aware components that can be used economically to
    keep your templating DRY.
-   I.e. *layouts/partials/func/GetFeaturedImage.html* will retrieve an image inside
    *theNewDynamic/gohugo-theme-ananke/layouts/partials/func/GetFeaturedImage.html*
    a _page bundle_, which file name contains `*cover*`.
-   I.e. `[layouts/partials/docs/html-head]` includes the stylesheet links (scss), and is
    pointing to its relative paths.
{{< highlight html >}}
{{< readfile file="_vendor/github.com/alex-shpak/hugo-book/layouts/partials/docs/html-head.html" markdown="true" >}}
{{< /highlight >}}
    
    -   `$styles.RelPermalink` will in turn look for the various scss sources as
        defined in `[assets/book.scss]`
{{< highlight scss >}}
{{< readfile file="/_vendor/github.com/alex-shpak/hugo-book/assets/book.scss" markdown="true" >}}
{{< /highlight >}}

## hugo-books Theme {#hugo-books-theme}

-   This seems to be most suitable for general-purpose technical documentations


### Find Essential Components {#find-essential-components}

-   Navbar

{{< highlight html >}}
{{< readfile file="/_vendor/github.com/alex-shpak/hugo-book/layouts/partials/docs/menu.html" markdown="true" >}}
{{< /highlight >}}

-   [Docs Page Template](i.e. will use partials/docs/html-head)

{{< highlight html >}}
{{< readfile file="/_vendor/github.com/alex-shpak/hugo-book/layouts/_default/baseof.html" markdown="true" >}}
{{< /highlight >}}

-   <span class="underline">NOTE</span>: _Page-Template_ replaces both _list-template_ and _single-template_ in this theme
-   Check if [layouts/\_default/baseof.html](https://gohugo.io/hugo-modules/theme-components/) conflicts with org-mode
    -   If there is a conflict simply use use [ox-hugo](https://ox-hugo.scripter.co/)

        ```bash
          75 {{ define "main" }}
          76   <article class="markdown">  ## <-- most likely only relevant for scss
          77     {{- .Content -}}
          78   </article>
          79 {{ end }}
        ```


## General {#general}


### Structure {#structure}

-   Below find the standard mechanics for adding static content/themes

```bash
hugo-bare/
├── archetypes
│   └── default.md
├── config.toml
├── content
│   └── posts             ## <-- hugo new posts/my-first-post.md
│       └── my-first-post.md
├── data
├── layouts
├── static
└── themes

# git submodule add <clone/from/github.com> themes/ananke
hugo-getstarted/
├── archetypes
├── content
├── data
├── layouts
├── static
└── themes
    └── ananke
        ├── archetypes
        ├── assets
        │   └── ananke
        │       ├── css
        │       └── dist
        ├── exampleSite
        │   ├── content
        │   │   ├── en
        │   │   │   ├── about
        │   │   │   └── post
        │   │   └── fr
        │   │       └── post
        │   └── static
        │       └── images
        ├── i18n
        ├── images
        ├── layouts
        │   ├── _default
        │   ├── page
        │   ├── partials
        │   │   ├── func
        │   │   │   └── style
        │   │   └── svg
        │   ├── post
        │   └── shortcodes
        ├── resources
        │   └── _gen
        │       └── assets
        │           └── css
        │               └── ananke
        │                   └── css
        └── static
            └── images
```


## FAQs {#faqs}


### 1. What are ideal theme components for structuring a technical documentation? {#1-dot-what-are-ideal-theme-components-for-structuring-a-technical-documentation}

..


### 2. [hugo-books] What is the relationship between theme/assets and resources? {#2-dot-hugo-books-what-is-the-relationship-between-theme-assets-and-resources}

-   `_gen/assets/scss/book.scss` seems to be automatically derived, wheras it is
    indicated that there is a possibility of overwriting the scss.
-   See comment: ['you can customize scss by creating \`assets/custom.scss\` in your website [..]']()

{{< highlight html >}}
{{< readfile file="/_vendor/github.com/alex-shpak/hugo-book/layouts/partials/docs/html-head.html" markdown="true" >}}
{{< /highlight >}}

..


### 3. [hugo-books] What does layouts/partials/docs/inject do? {#3-dot-hugo-books-what-does-layouts-partials-docs-inject-do}

-   All files in this directory are empty?


## Where To Go From Here? {#where-to-go-from-here}

-   [ ] Understand Hugo concepts
    -   [ ] Structure, Themes, Content Management, Templates, Functions .. implement
        at least one custom of each
    -   [ ] How is the underlying Go integrated with the front-end template engine?
    -   [ ] Dive into Hugo source code to evaluate the possibility of re-purpose for
        scss/js combining with Beego.
-   [ ] Add an `.org` mode example `docs` as well as `posts`
-   [ ] Do a custom logo
-   [ ] Evaluate docs Theme: [https://github.com/alex-shpak/hugo-book](https://github.com/alex-shpak/hugo-book) (adding
    matomo support: [https://github.com/hotosm/hugo-book](https://github.com/hotosm/hugo-book))
-   [ ] Combine multiple themes
    ([https://gohugo.io/hugo-modules/theme-components/](https://gohugo.io/hugo-modules/theme-components/)), i.e. Use a different Theme
    for both the Cover Page as well About, whereas docs remain in _hugo-book_
-   [ ] Create the documentation for biscari0dev
-   [ ] Dockerize and prepare for use with Nomad
-   [ ] Run Hugo along with Beego
-   [ ] Create a healthz route for the Hugo page
-   [ ] Create real content and complete dev pipeline for docs
